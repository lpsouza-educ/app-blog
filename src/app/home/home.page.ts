import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';

interface IPost {
  id: number;
  title: string;
  body: string;
  userId: number;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [
    Geolocation
  ]
})
export class HomePage {

  api: string;
  posts: IPost[];
  latitude: number;
  longitude: number;

  constructor(
    public httpClient: HttpClient,
    public geolocation: Geolocation
  ) {
    this.api = 'https://jsonplaceholder.typicode.com';
    
    // Obtenção dos posts da API
    httpClient.get(this.api + '/posts')
      .subscribe((res: IPost[]) => {
        console.log(res);
        this.posts = res;

        // Exemplo demonstrando que é possível modificar os dados do objeto criado a partir do JSON
        // this.posts.forEach(post => {
        //   if (post.id === 1) {
        //     post.title = "Modificado pelo sor";
        //   }
        // })
      });

    // Obtenção das coordenadas no momento da requisição
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      }).catch((error) => {
        console.log('Erro ao obter a localização', error);
      });

    // Obtenção das coordenadas e mantendo um controle onde fica re-obtendo quando há mudanças
    // let watch = this.geolocation.watchPosition();
    // watch.subscribe((data) => {
    //   this.latitude = data.coords.latitude
    //   this.longitude =  data.coords.longitude
    // });
  }

}
